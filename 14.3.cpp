
#include <iostream>
#include <string>
#include <iomanip>
#include <stdint.h>

std::string GlobalString;
int GlobalInt = 0;
bool GLobalBool = true;



int ActionToDo(int ActionInt)
{
    std::cout << "\nActiontoDo function: access\n"; // testing getting in functuion

    switch (ActionInt)
    {
    case 1: 
        std::cout << "\nFirst letter of your string is " << GlobalString.at(0) << "\n";
        break;
    case 2: 
        std::cout << "\nLast letter of your string is " << GlobalString.at(GlobalString.length()-1) << "\n";
        break;
    case 3: 
        if (GlobalString.length() == 1) //check if string contains only one letter to set the correct answer on case 3
        {
             std::cout << "\nYour string contains " << GlobalString.length() << " letter \n";
        }
        else
        {
            std::cout << "\nYour string contains " << GlobalString.length() << " letters \n";
        }
        break;
    case 4: 
        GLobalBool = false; //get out of the cycle in main function and stop program
        break;
    }
    return 0;
}

int main()
{
    //Set local variables
    std::string MyString, FirstLetter, LastLetter;

    // String input request
    std::cout << "Please Enter Your String\n";
    std::getline(std::cin, MyString);
    GlobalString = MyString;
    std::cout << "\nYour string: " << MyString << "\n\n";
    
    //Cycle until number 4've been selected to stop program
    while (GLobalBool == true)
    {
        std::cout << "\nWhat do you want to do next?\n" << "Enter 1 to show first letter\n" << "Enter 2 to show last letter\n";
        std::cout << "Enter 3 to show number of letters\n" << "Enter 4 to close program\n";

        std::cin >> GlobalInt;
       
        
        if (GlobalInt == 1 || GlobalInt == 2 || GlobalInt == 3 || GlobalInt == 4)
        {
            ActionToDo(GlobalInt);
        }
        else
        {
            std::cout << "\nInvalid command\n";
        }   
    }    
return 0;
}

